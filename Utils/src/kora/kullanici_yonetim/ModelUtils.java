package kora.kullanici_yonetim;

import Utils.ADFUtils;

import java.util.Map;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.VariableValueManager;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewObject;

public class ModelUtils {
    public ModelUtils() {
        super();
    }
    
    public static void applyViewCriteria(String iteratorName, String viewCriteriaName, Map<String, Object> variableMap) {
        DCIteratorBinding binding = ADFUtils.findIterator(iteratorName);
        ViewObject viewObject = binding.getViewObject();
        ViewCriteriaManager viewCriteriaManager = viewObject.getViewCriteriaManager();
        ViewCriteria viewCriteria = viewCriteriaManager.getViewCriteria(viewCriteriaName);
        VariableValueManager ensureVariableManager = viewCriteria.ensureVariableManager();
        for (String key : variableMap.keySet()) {
            ensureVariableManager.setVariableValue(key, variableMap.get(key));
        }
        viewObject.applyViewCriteria(viewCriteria);
        viewObject.executeQuery();
    }
}
