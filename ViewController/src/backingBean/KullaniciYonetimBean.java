package backingBean;

import Utils.ADFUtils;
import Utils.JSFUtils;

import Utils.PasswordUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import kora.kullanici_yonetim.ModelUtils;
import kora.kullanici_yonetim.model.AppModuleImpl;
import kora.kullanici_yonetim.model.view.YmsuCustomersGrupVViewRowImpl;
import kora.kullanici_yonetim.model.view.YpouUsersViewRowImpl;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

import oracle.sqlj.runtime.Oracle;

import org.apache.myfaces.trinidad.model.RowKeySet;

public class KullaniciYonetimBean {
    private static final String YpouUsersViewCriteriaIterator = "YpouUsersViewCriteriaIterator";
    private static final String YpouUsersViewIterator = "YpouUsersViewIterator";
    private static final String YmsuCustomersGrupVViewIterator = "YmsuCustomersGrupVViewIterator";
    private static final String YpouUsersForMagazaYoneticiViewIterator = "YpouUsersForMagazaYoneticiViewIterator";
    private static final String YpouCalisanListesiViewIterator = "YpouCalisanListesiViewIterator";
    private static final String YpouUsersForYoneticiIterator = "YpouUsersForYoneticiIterator";


    private boolean ekleme;
    private Object personName;
    private String customerName;
    private RichInputText adSoyadText;
    private RichInputComboboxListOfValues kaydedilecekKullanici;
    private RichInputComboboxListOfValues lov_Ad_Soyad;
    private RichInputComboboxListOfValues lov_UserName;
    private Boolean bayiMi = false;
    private YpouUsersViewRowImpl bayiYoneticisiRow;
    private boolean tamYetki = false;
    private boolean magazaYetki = false;

    public KullaniciYonetimBean() {

    }

    public String setCurrentRowWithPersonId() {
        if (lov_Ad_Soyad.getValue() == null && lov_UserName.getValue() == null &&
            JSFUtils.resolveExpression("#{pageFlowScope.personId}") == null &&
            JSFUtils.resolveExpression("#{pageFlowScope.uName}") == null) {
            JSFUtils.addFacesErrorMessage("L�tfen �nce kullan?c?y? se�iniz.");
            return "UserYok";
        }
        ekleme = false;

        if (kaydedilecekKullanici != null) {
            kaydedilecekKullanici.setDisabled(true);
        }

        Object personId = JSFUtils.resolveExpression("#{pageFlowScope.personId}");
        Object uName = JSFUtils.resolveExpression("#{pageFlowScope.uName}");
        if (uName == null) {
            Map<String, Object> variableMap = new HashMap<String, Object>();
            variableMap.put("personId", personId);
            DCIteratorBinding iterator = ADFUtils.findIterator(YpouUsersViewCriteriaIterator);
            System.out.println(iterator.getViewObject());
            ModelUtils.applyViewCriteria(YpouUsersViewCriteriaIterator, "ByPersonId", variableMap);
            YpouUsersViewRowImpl currentRow =
                (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewCriteriaIterator).getCurrentRow();
            if (currentRow == null) {
                FacesMessage message = new FacesMessage("Kullan?c? Kay?tl? De?ildir!");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return "UserYok";
            }
            ADFUtils.findIterator(YpouUsersViewIterator).setCurrentRowWithKeyValue(currentRow.getUName());
            System.out.println(currentRow.getCustomerId());
            setCustomerName(currentRow.getCustomerName());
            setPersonName(currentRow.getUFirstName() + " " + currentRow.getULastName());
            return "";
        } else {
            Map<String, Object> variableMap = new HashMap<String, Object>();
            variableMap.put("uName", uName);
            DCIteratorBinding iterator = ADFUtils.findIterator(YpouUsersViewCriteriaIterator);
            System.out.println(iterator.getViewObject());
            ModelUtils.applyViewCriteria(YpouUsersViewCriteriaIterator, "ByUName", variableMap);
            YpouUsersViewRowImpl currentRow =
                (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewCriteriaIterator).getCurrentRow();
            if (currentRow == null) {
                FacesMessage message = new FacesMessage("Kullan?c? Kay?tl? De?ildir!");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return "UserYok";
            }
            ADFUtils.findIterator(YpouUsersViewIterator).setCurrentRowWithKeyValue(currentRow.getUName());
            System.out.println(currentRow.getCustomerId());
            setCustomerName(currentRow.getCustomerName());
            setPersonName(currentRow.getUFirstName() + " " + currentRow.getULastName());
            return "";
        }
    }


    public void satisTemsilcisiForUserNameChangeListener(ValueChangeEvent valueChangeEvent) {
        //ADFUtils.findControlBinding("UName").setInputValue(valueChangeEvent.getNewValue());
        if (valueChangeEvent.getNewValue().equals(""))
            return;
        OperationBinding binding = ADFUtils.findOperation("setCurrentRowWithKeyValue");
        binding.getParamsMap().put("rowKey", valueChangeEvent.getNewValue());
        binding.execute();
        /*valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Number personId = (Number)ADFUtils.findControlBinding("PersonId1").getInputValue();
        System.out.println(personId);*/
        ADFUtils.setEL("#{pageFlowScope.uName}", valueChangeEvent.getNewValue());
    }

    public void satisTemsilcisiChangeListener(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue().equals(""))
            return;
        System.out.println(valueChangeEvent.getNewValue());
        ADFUtils.findControlBinding("Tamad").setInputValue(valueChangeEvent.getNewValue());
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        Number personId = (Number)ADFUtils.findControlBinding("PersonId").getInputValue();
        System.out.println(ADFUtils.findControlBinding("Tamad").getInputValue());
        if (ADFUtils.findControlBinding("UName") != null) {
            YpouUsersViewRowImpl currentRow =
                (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewIterator).getCurrentRow();
            Object personIdForDuzenle = ADFUtils.findControlBinding("PersonId1").getInputValue();
            String[] fullName = valueChangeEvent.getNewValue().toString().split(" ");
            String firstName = fullName[0];
            String lastName = fullName[1];
            if (fullName.length == 3) {
                lastName += " " + fullName[2];
            }
            if (ADFUtils.findControlBinding("SalesrepId") != null) {
                Object salesrepId = ADFUtils.findControlBinding("SalesrepId").getInputValue();
                currentRow.setOracleSalesrepId((Number)salesrepId);
            }
            currentRow.setUFirstName(firstName);
            currentRow.setULastName(lastName);
            currentRow.setPersonId((Number)personIdForDuzenle);
        }

        ADFUtils.setEL("#{pageFlowScope.personId}", personId);
    }
    
    public void btDebug(ActionEvent actionEvent) {
        
        System.out.println("Ehehe");
    }

    public void btKaydetActionListener(ActionEvent actionEvent) {
        YpouUsersViewRowImpl row = (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewIterator).getCurrentRow();
        row.setUName(row.getUName().toUpperCase());
        //row.setUPassword("NEWUSERPASSWORD");
        if (bayiMi == true) { // Giris yapan kullanici bayi mi
            row.setMagazaBayi("BAYI");
            if (!tamYetki)
               row.setPersonId(bayiYoneticisiRow.getPersonId());
            
            YmsuCustomersGrupVViewRowImpl rowCust =
                (YmsuCustomersGrupVViewRowImpl)ADFUtils.findIterator(YmsuCustomersGrupVViewIterator).getCurrentRow();
            
            row.setCustomerId(rowCust.getCustomerId());//(bayiYoneticisiRow.getCustomerId());
            row.setCustomerName(rowCust.getCustomerName());//(bayiYoneticisiRow.getCustomerName());
            row.setOracleSalesrepId(bayiYoneticisiRow.getOracleSalesrepId());
            row.setOrgId(rowCust.getOrgId());//(bayiYoneticisiRow.getOrgId());
            row.setSehir(bayiYoneticisiRow.getSehir());
            if (!adSoyadText.getValue().equals("")) {
                // AdSoyad = Ad + Soyad olacak sekilde 2 ye ayir
                // ve UFirstName ile ULastName'e set et
                String[] fullName = adSoyadText.getValue().toString().split(" ");
                String firstName = fullName[0];
                row.setUFirstName(firstName);
                if (fullName.length > 1) {
                    String lastName = fullName[1];
                    if (fullName.length == 3) {
                        lastName += " " + fullName[2];
                    }
                    row.setULastName(lastName);
                }
            }
        }
        
        // Kisinin yoneticisini bul
        OperationBinding operation = ADFUtils.getBindingContainer().getOperationBinding("ExecuteWithParams");
        Map params = operation.getParamsMap();
        params.put("customerId", ADFUtils.findControlBinding("CustomerId"));
        operation.execute();
        
        // Bulunan satiri al ve kisinin yoneticisini set et
        YpouUsersViewRowImpl rowForParent =
            (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersForMagazaYoneticiViewIterator).getCurrentRow();
        if (rowForParent != null)
            row.setUParentUser(rowForParent.getUName());
        
        
        AppModuleImpl module = (AppModuleImpl)ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        if (module.userExists(row.getUName())) { // Bu kismin ikiye ayrilmasi gerekiyor. Yeni ekle seklinde bir kayitsa boyle olmamali.
            //JSFUtils.addFacesErrorMessage("Kullan?c? zaten ekli, g�ncellemeleri d�zenle ekran?ndan yap?n?z.");
            row.setUpdatedBy(getCurrentUser());
            row.setUpdatedDate(new oracle.jbo.domain.Timestamp(new java.util.Date().getTime()));
        } else {
            row.setCreatedBy(getCurrentUser());
            row.setCreatedDate(new oracle.jbo.domain.Timestamp(new java.util.Date().getTime()));
            
            OperationBinding createWeblogicUser = ADFUtils.getBindingContainer().getOperationBinding("createUser");
            String password = PasswordUtils.generateInitialPassword();
            row.setUPassword(password);
            String eMail = row.getEMail();
            Map paramsForWeblogic = createWeblogicUser.getParamsMap();
            paramsForWeblogic.put("userId", row.getUName());
            paramsForWeblogic.put("userPassword", password);
            paramsForWeblogic.put("userDescription", row.getUDescription());
            paramsForWeblogic.put("eMail", eMail);
            
            ArrayList groupNames = new ArrayList();
            if (row.getMagazaBayi().equals("BAYI")) { // Kullanicinin hangi gruplara eklenecegini belirle
                groupNames.add("YatasBayi");
            } else {
                groupNames.add("YatasCalisan");
            }
            paramsForWeblogic.put("groupNames", groupNames);
            
            createWeblogicUser.execute();
            if (createWeblogicUser.getErrors().isEmpty()) {
                sendInitialMail(eMail, password, adSoyadText.getValue().toString());
            } else {
                FacesMessage message = new FacesMessage("Kullanici eklenirken hata olustu.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
    }

    public String Commit() {
        OperationBinding operation = ADFUtils.findOperation("Commit");
        operation.execute();
        if(!operation.getErrors().isEmpty()){
            JSFUtils.addFacesErrorMessage("Kay?tl? kullan?c?y? g�ncellemeyi d�zenle sayfas?ndan yapabilirsiniz.");
            return "hata";
        }
        return "ok";
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void customerChangeListener(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue().equals(""))
            return;
        ADFUtils.findIterator(YmsuCustomersGrupVViewIterator);
        Map<String, Object> variableMap = new HashMap<String, Object>();
        variableMap.put("customerName", valueChangeEvent.getNewValue());
        ModelUtils.applyViewCriteria(YmsuCustomersGrupVViewIterator, "ByCustomerName", variableMap);
        YmsuCustomersGrupVViewRowImpl row =
            (YmsuCustomersGrupVViewRowImpl)ADFUtils.findIterator(YmsuCustomersGrupVViewIterator).getCurrentRow();
        System.out.println(row.getCustomerId());
        System.out.println(row.getCustomerName());
        ADFUtils.findControlBinding("CustomerId").setInputValue(row.getCustomerId());
        ADFUtils.findControlBinding("OrgId").setInputValue(row.getOrgId());
        System.out.println(ADFUtils.findControlBinding("OrgId"));
        System.out.println(ADFUtils.findControlBinding("SalesrepId"));
    }


    public void weblogicUserSil_Listener(ActionEvent actionEvent) {
        OperationBinding weblogicUserSil = ADFUtils.getBindingContainer().getOperationBinding("removeSelectedUser");
        Map param = weblogicUserSil.getParamsMap();
        param.put("username", ADFUtils.findControlBinding("UName"));
        weblogicUserSil.execute();
        if (weblogicUserSil.getErrors().isEmpty()) {
            YpouUsersViewRowImpl row =
                (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewIterator).getCurrentRow();
            System.out.println(row.getUName());
            OperationBinding createInsert = ADFUtils.getBindingContainer().getOperationBinding("CreateInsert");
            createInsert.execute();
            YpouUsersViewRowImpl currentRow =
                (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewIterator).getCurrentRow();
            System.out.println(currentRow.getUName());
            currentRow = row;
            row.setActive("f");
            OperationBinding passivate = ADFUtils.getBindingContainer().getOperationBinding("Commit");
            passivate.execute();
        }
    }

    private void sendInitialMail(String mail, String password, String adSoyad) {
        String to = mail;     
        String from = "oracle@portal.yatas.com.tr";
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "mail.yatas.com.tr");
        properties.setProperty("mail.smtp.port", "25");
        properties.setProperty("mail.smtp.auth", "false");
        
        Session session = Session.getInstance(properties);
            /*,
           new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication(
                    "oracle.alert@yatas.com.tr", "Oracle44u");
              }
           });*/

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Yatas Kullanici Parolasi");
            message.setText("Sayin " + adSoyad + ", Yatas Sistemleri Icin Giris Sifreniz: '" + password +
                            "' seklindedir. iyi g�nler.");
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
    
    public void weblogicIsleri() {
        
    }

    public void userActivate_Listener(ActionEvent actionEvent) {
        YpouUsersViewRowImpl row =
            (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersViewIterator).getCurrentRow();
        OperationBinding createWeblogicUser = ADFUtils.getBindingContainer().getOperationBinding("createUser");
        String password = PasswordUtils.generateInitialPassword();
        String eMail = row.getEMail();
        Map params = createWeblogicUser.getParamsMap();
        params.put("userId", row.getUName());
        params.put("eMail", eMail);
        params.put("firstName", row.getUFirstName());
        params.put("lastName", row.getULastName());
        params.put("userPassword", password);
        
        ArrayList groupNames = new ArrayList();
        if (row.getMagazaBayi().equals("BAYI")) { // Kullanicinin hangi gruplara eklenecegini belirle
            groupNames.add("YatasBayi");
        } else {
            groupNames.add("YatasCalisan");
        }
        params.put("groupNames", groupNames);
        
        createWeblogicUser.execute();
        if (createWeblogicUser.getErrors().isEmpty()) {
            sendInitialMail(eMail, password, adSoyadText.getValue().toString());
            row.setUPassword(password);
            row.setActive("t");
            ADFUtils.getBindingContainer().getOperationBinding("Commit").execute();
        } else {
            FacesMessage message = new FacesMessage("Kullan?c? Aktive Edilirken Bir Hatayla Kar??la??ld?.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public String yoneticiMi() {
        YpouUsersViewRowImpl currentRow =
            (YpouUsersViewRowImpl)ADFUtils.findIterator(YpouUsersForYoneticiIterator).getCurrentRow();
        System.out.println(currentRow.getTabletMagazaYoneticisiMi());
        System.out.println(currentRow.getUName());
        if ("2".equals(currentRow.getTabletMagazaYoneticisiMi())) {
            setTamYetki(true);
            setBayiYoneticisiRow(currentRow);
            ADFUtils.setEL("#{pageFlowScope.customerId}", currentRow.getCustomerId());
            return "true";
        }
        if ("3".equals(currentRow.getTabletMagazaYoneticisiMi())) {
            setMagazaYetki(true);
            return "true";
        }
        if ("1".equals(currentRow.getTabletMagazaYoneticisiMi())) {
            if (currentRow.getMagazaBayi().equals("BAYI")) {
                setBayiMi(true);
                setBayiYoneticisiRow(currentRow);
            }
            ADFUtils.setEL("#{pageFlowScope.customerId}", currentRow.getCustomerId());
            return "true";
        }
        return "false";
    }

    public void kullaniciGir_Listener(ActionEvent actionEvent) {
        adSoyadText.setVisible(true);
        kaydedilecekKullanici.setDisabled(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(adSoyadText);
        AdfFacesContext.getCurrentInstance().addPartialTarget(kaydedilecekKullanici);
    }

    public void setAdSoyadText(RichInputText adSoyadText) {
        this.adSoyadText = adSoyadText;
    }

    public RichInputText getAdSoyadText() {
        return adSoyadText;
    }

    public void setBayiMi(Boolean bayiMi) {
        this.bayiMi = bayiMi;
    }

    public Boolean getBayiMi() {
        return bayiMi;
    }

    public void setKaydedilecekKullanici(RichInputComboboxListOfValues kaydedilecekKullanici) {
        this.kaydedilecekKullanici = kaydedilecekKullanici;
    }

    public RichInputComboboxListOfValues getKaydedilecekKullanici() {
        return kaydedilecekKullanici;
    }

    public void yeniEkle_Listener(ActionEvent actionEvent) {
        // Add event code here...
        ekleme = true;
        setPersonName(null);
        if (kaydedilecekKullanici != null) {
            kaydedilecekKullanici.setDisabled(false);
        }

        if (customerName != null)
            customerName = "";
        ADFUtils.findControlBinding("Tamad").setInputValue("");
    }

    public String getCurrentUser() {
        return ADFContext.getCurrent().getSecurityContext().getUserName();
    }

    public void setBayiYoneticisiRow(YpouUsersViewRowImpl bayiYoneticisiRow) {
        this.bayiYoneticisiRow = bayiYoneticisiRow;
    }

    public YpouUsersViewRowImpl getBayiYoneticisiRow() {
        return bayiYoneticisiRow;
    }

    public void setLov_Ad_Soyad(RichInputComboboxListOfValues lov_Ad_Soyad) {
        this.lov_Ad_Soyad = lov_Ad_Soyad;
    }

    public RichInputComboboxListOfValues getLov_Ad_Soyad() {
        return lov_Ad_Soyad;
    }

    public void setLov_UserName(RichInputComboboxListOfValues lov_UserName) {
        this.lov_UserName = lov_UserName;
    }

    public RichInputComboboxListOfValues getLov_UserName() {
        return lov_UserName;
    }

    public void rowEditListener(ActionEvent actionEvent) {
        if (actionEvent.getComponent().getAttributes().get("personId") != null)
            ADFUtils.setEL("#{pageFlowScope.personId}", actionEvent.getComponent().getAttributes().get("personId"));
        else
            ADFUtils.setEL("#{pageFlowScope.uName}", actionEvent.getComponent().getAttributes().get("UName"));
    }

    public void satisKanaliChangeListener(ValueChangeEvent valueChangeEvent) {
        Object newValue = valueChangeEvent.getNewValue();
        
        // Sezer senin ta.
        bayiMi = newValue.equals("BAYI");
        
        if (ekleme) {
            if (newValue.equals("MAGAZA")) {
                kaydedilecekKullanici.setVisible(true);
                adSoyadText.setVisible(false);
            } else {
                adSoyadText.setVisible(true);
                kaydedilecekKullanici.setVisible(false);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(kaydedilecekKullanici);
        AdfFacesContext.getCurrentInstance().addPartialTarget(adSoyadText);
    }

    public String redirect() {
        JSFUtils.redirectPage("KullaniciYonetim.jspx");
        return "";
    }

    public boolean isEkleme() {
        return ekleme;
    }

    public Object getPersonName() {
        return personName;
    }

    public void setPersonName(Object personName) {
        this.personName = personName;
    }

    public void setTamYetki(boolean tamYetki) {
        this.tamYetki = tamYetki;
    }

    public boolean isTamYetki() {
        return tamYetki;
    }

    public void setMagazaYetki(boolean magazaYetki) {
        this.magazaYetki = magazaYetki;
    }

    public boolean isMagazaYetki() {
        return magazaYetki;
    }
}
